#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <aSubRecord.h>

#include "freq_tracking.h"


/*
 * Include New element on calibration tables
 * 
 * Parameters:
 * A: Enable pulse generation
 * C: Enable tracking
 * D: Delta_f
 * E: Phase
 * F: Table size
 * G: Sampling rate
 *
 * Output:
 * A: FF Angle waveform
 */
//
static long freq_tracking(aSubRecord *psr){
    short const gen_enabled      =  *((short*)  psr->a);
    long angle_cur_size          =  *((long*)   psr->b);
    short const tracking_enabled =  *((short*)  psr->c);
    double delta_f               =  *((double*) psr->d);
    double const phase           =  *((double*) psr->e);
    long const table_size        =  *((long*)   psr->f);
    double const fsampling       = (*((double*) psr->g))*1e6;
    double const IQN             =  *((double*) psr->h);

    double const sampling_rate = fsampling / IQN;

    // If generation not enable, don't touch table
    if(gen_enabled == 0) {
        psr->neva = angle_cur_size;
        return 0;
    }

    // If tracking not enabled, over-write detuning value
    if (tracking_enabled == 0) {
        delta_f = 0;
    }

    double * angles = (double*)psr->vala;
    long i;
    for (i = 0; i < table_size; i++) {
        angles[i] = -2*M_PI*delta_f/sampling_rate*i + phase;
        angles[i] = 2*fmod(angles[i], M_PI) - fmod(angles[i], 2*M_PI);
    }
    psr->neva = table_size;

    return 0;
}


/*
 * Include New element on calibration tables
 * 
 * Parameters:
 * A: Enable
 * B: Old table size
 * C: Sampling rate
 * D: Table size
 * E: Slope length
 * F: Pre-pulse length
 * G: Pre-pulse power ratio to pulse power
 * H: Pulse power
 *
 * Output:
 * A: FF Magnitude waveform
 */
//
static long pulse_gen(aSubRecord *psr){
    short  const gen_enabled   =  *((short*)  psr->a);
    long   const mag_cur_size  =  *((long*)   psr->b);
    double const fsampling     =  *((double*) psr->c);
    double const IQN           =  *((double*) psr->d);
    long   const table_size    =  *((long*)   psr->e);
    double const Tslope        = (*((double*) psr->f))*1e3;
    double const Tfill         = (*((double*) psr->g))*1e3;
    double const Pfill_ratio   =  *((double*) psr->h);
    double const P             =  *((double*) psr->i);

    // If generation not enabled, don't touch table.
    if(gen_enabled == 0) {
        psr->neva = mag_cur_size;
        return 0;
    }

    // Error conditions
    if(table_size < 1 ||
       Tslope < 0 ||
       Tfill < 0 ||
       P < 0) {
      return 1;
    }

    double const sampling_rate = fsampling / IQN;

    double Pfill;
    // Only allow higher powers in pre-pulse than in main pulse
    if(Pfill_ratio < 1) { 
        Pfill = P;
    } else {
        Pfill = P * Pfill_ratio;
    }

    // Calculate end point of each pulse segment and check for
    //  overrun with table size
    long i_end_slope = (long)(Tslope * sampling_rate);
    if(i_end_slope > table_size) i_end_slope = table_size;

    long i_end_fill  = (long)((Tfill + Tslope) * sampling_rate);
    if(i_end_fill > table_size) i_end_fill = table_size;

    long i = 0;
    double * mag = (double*)psr->vala;

    // Fill magnitude with slope up to Pfill in Tfill
    for (i = 0; i < i_end_slope; i++) {
        mag[i] = (Pfill / i_end_slope) * i;
    }
    // Fill magnitude with constant power Pfill in pre-pulse
    for (i = i_end_slope; i < i_end_fill; i++) {
        mag[i] = Pfill;
    }
    // Fill magnitude with constant power P in pulse
    for (i = i_end_fill; i < table_size; i++) {
        mag[i] = P;
    }
    // Set the last point to P to avoid that the filling power
    //  is set along the full pulse should the table be too short
    mag[table_size - 1] = P;

    psr->neva = table_size;

    return 0;
}

// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(freq_tracking);
epicsRegisterFunction(pulse_gen);
