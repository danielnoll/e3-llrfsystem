# This should be the included snippet to configure and run the deployed IOC. It should be loaded with
#
#   iocshLoad("$(llrfsystem_DIR)/ts2.iocsh")

iocshLoad("$(llrfsystem_DIR)/base.iocsh")
iocshLoad("$(sis8300llrf_DIR)/sis8300llrf-2-boards.iocsh")

#Detune for 1st cavity
dbLoadRecords("detune.template", "P=$(P), R=$(R), PD=$(PD1), RD=$(RD1), CAVITYCH=$(CAVITYCH1), CAVFWDCH=$(CAVFWDCH1), CH=1, RFENDPV=$(RFENDPV)")
#Detune for 2nd cavity
dbLoadRecords("detune.template", "P=$(P), R=$(R), PD=$(PD2), RD=$(RD2), CAVITYCH=$(CAVITYCH2), CAVFWDCH=$(CAVFWDCH2), CH=2, RFENDPV=$(RFENDPV)")

##### Support records to calibration management
# Records to generate ramps for SP and FF
# TODO change for substitution files?
dbLoadRecords("gen-pi-table.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), PI_TYPE=SP, PITABLE_SMNM_MAX=$(SP_SMNM_MAX)")
dbLoadRecords("gen-pi-table.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), PI_TYPE=FF, PITABLE_SMNM_MAX=$(FF_SMNM_MAX)")

# records to copy calibration to SP/FF
dbLoadRecords("copy-pi-calib-tables.db", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(TABLE_SMNM_MAX), CAVITYCH=$(CAVITYCH), PWRFWDCH=$(PWRFWDCH)")


##### Select correct calibration for Set-Point depending on Cavity/Without Cavity
##### Set-Point is only used when in Closed Loop

# sequencer for change the tables depending on cavity/without cavitya
dbLoadRecords("sp-calib-sel.db", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), CAVITYCH=$(CAVITYCH), PWRFWDCH=$(PWRFWDCH)")
seq("spcalibsel", "P=$(P), R=$(R)")

###### Select the right calibration for FF
###### The calibration table for FF depends on if Closed/Open Loop and if is With/Without cavity
seq("picalibsel", "P=$(P), R=$(R), PI_TYPE=FF")
# Automatic calibration for FF
dbLoadRecords("autocalib-ff.db", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(TABLE_SMNM_MAX), CAVITYCH=$(CAVITYCH), PWRFWDCH=$(PWRFWDCH)")
seq("ffautocalib", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(TABLE_SMNM_MAX)")

dbLoadRecords("monitor_ilock.template", "P=$(P), R=$(R), PM=$(PD), RM=$(RD), PS=$(PD), RS=$(LLRF_DIG_R_2):, CH=2")

# Frequency tracking record
dbLoadRecords("freq-tracking.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD),PI_TYPE=FF, TABLE_SMNM_MAX=$(FF_SMNM_MAX), RR=$(RR)")

# Restore autosaved values
dbLoadRecords("llrfRestoreDetuneTS2.template", "P=$(P), R=$(R)")
dbLoadRecords("llrfRestore.template", "P=$(P), R=$(R)")
afterInit("dbpf $(P)$(R)#RestoreLLRF 1")

# Reload last tables
afterInit("dbpf $(PD)$(RD)FFTbl-Mag.PROC 1")
afterInit("dbpf $(PD)$(RD)FFTbl-Ang.PROC 1")
afterInit("dbpf $(PD)$(RD)FFTbl-TblToFW 1")

afterInit("dbpf $(PD)$(RD)SPTbl-Mag.PROC 1")
afterInit("dbpf $(PD)$(RD)SPTbl-Ang.PROC 1")
afterInit("dbpf $(PD)$(RD)SPTbl-TblToFW 1")

afterInit("dbpf $(P)$(R)#FFPulseGen.DISA 0") # Enable FreqTrack Pulse Gen

# Load labels
iocshLoad("$(llrfsystem_DIR)/ts2_labels.iocsh")
